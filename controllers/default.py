import os, io
from PIL import Image
from time import time
from datetime import datetime
import json

# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

# ---- example index page ----
def index():
    return dict()

def survey():
    return dict()

def avatar():
    return dict()

def choice():
    return dict()

def choice_avatar():
    return dict()

def choice_avatar_color():
    return dict()

def done():
    return dict()

def submit():
    db.trees.insert(json_data=json.dumps(request.vars))
    return "Done"

def credits():
    return dict()

def submit_email():
    db.trees_to_email.insert(email=request.args[0],json_data=json.dumps(request.vars))
    return "Done"

def bigscreen():
    trees = db(db.trees.id>0).select(orderby=~db.trees.id)
    return dict(trees=trees)

def get_images():
    last_check = int(request.vars.since)
    scale = float(request.vars.scale)
    trees = db(db.trees.id>last_check).select(orderby=~db.trees.id)
    if len(trees) > 0:
        response.headers["X-LARGEST-ID"] = trees[0].id
    else:
        response.headers["X-LARGEST-ID"] = last_check
    return dict(trees=trees,scale=scale)

class DataTree():
    def __init__(self):
        self.image = self.get_image("base")

    def get_image(self,f):
        return Image.open(os.path.join(request.folder,"layers","%s.png" % f))

    def composite_image(self,question,answer):
        if request.vars.get(question) == answer:
            self.image = Image.alpha_composite(self.image,self.get_image(question+"-"+answer))

    def composite_body(self):
        if request.vars.body:
            body = request.vars.body[5:]
            self.image = Image.alpha_composite(self.image,self.get_image("body-"+body))

    def composite_eyes(self):
        if request.vars.eyecolor and request.vars.eyestyle:
            eyecolor = request.vars.eyecolor[5:]
            eyestyle = request.vars.eyestyle[5:]
            self.image = Image.alpha_composite(self.image,self.get_image("eye-"+eyestyle+eyecolor))
        else:
            self.image = Image.alpha_composite(self.image,self.get_image("default"))

    def composite_hair(self):
        if request.vars.haircolor and request.vars.hairstyle:
            haircolor = request.vars.haircolor[5:]
            hairstyle = request.vars.hairstyle[5:]
            self.image = Image.alpha_composite(self.image,self.get_image("hair-"+hairstyle+haircolor))

    def composite_mouth(self):
        if request.vars.mouth:
            mouth = request.vars.mouth[5:]
            self.image = Image.alpha_composite(self.image,self.get_image("mouth-"+mouth))

    def composite_base(self):
        self.image = Image.alpha_composite(self.image,self.get_image("base"))
        if request.vars.food != "undefined":
            self.image = Image.alpha_composite(self.image,self.get_image("rope"))

    def stream_image(self):
        stream = io.BytesIO()
        self.image.save(stream,format="png")
        return stream

def image():
    start_time = time()
    image = DataTree()
    image.composite_image("animal","cat")
    image.composite_image("animal","octopus")
    image.composite_image("animal","bird")
    image.composite_image("animal","dog")
    image.composite_image("onleft","kiki")
    image.composite_image("onleft","bouba")
    image.composite_image("food","cheese")
    image.composite_image("food","brownies")
    image.composite_image("food","fries")
    image.composite_image("food","icecream")
    for question in ["toneofvoice","involuntarynoises","eyecontact","bored","routines","senses"]:
        for answer in ["often","sometimes","rarely","never"]:
            image.composite_image(question,answer)
    image.composite_base()
    image.composite_body()
    image.composite_image("bottom","pants")
    image.composite_image("bottom","skirt")
    image.composite_eyes()
    image.composite_hair()
    image.composite_mouth()
    image.composite_image("shirt","male")
    image.composite_image("shirt","female")
    stream = image.stream_image()
    end_time = time()
    seconds = end_time - start_time
    #return repr(seconds)
    response.headers["Content-Type"] = "image/png"
    if request.vars.download:
        response.headers["Content-Disposition"] = 'attachment; filename="datatree.png"'
    return stream.getvalue()

# ---- API (example) -----
@auth.requires_login()
def api_get_user_email():
    if not request.env.request_method == 'GET': raise HTTP(403)
    return response.json({'status':'success', 'email':auth.user.email})

# ---- Smart Grid (example) -----
@auth.requires_membership('admin') # can only be accessed by members of admin groupd
def grid():
    response.view = 'generic.html' # use a generic view
    tablename = request.args(0)
    if not tablename in db.tables: raise HTTP(403)
    grid = SQLFORM.smartgrid(db[tablename], args=[tablename], deletable=False, editable=False)
    return dict(grid=grid)

# ---- Embedded wiki (example) ----
def wiki():
    auth.wikimenu() # add the wiki to the menu
    return auth.wiki() 

# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

# ---- action to server uploaded static content (required) ---
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
